<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderDetail extends Model
{
  protected $table = 'purchase_orders_detail';
  protected $fillable = ['price', 'kg', 'products_id', 'cantidad', 'purchase_orders_id'];

   public function product(){
    return $this->belongsTo('App\Product', 'products_id', 'id');
  }
}
