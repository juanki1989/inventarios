<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('price')->nullable();
            $table->string('flete')->nullable();
            $table->string('state')->default('pendiente');
            $table->integer('customer_id')->nullable();
            $table->foreign('customer_id')
                  ->references('id')->on('customers')->onDelete('cascade');
            $table->integer('user_id')->nullable();
            $table->foreign('user_id')
                  ->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pruchase_orders');
    }
}
