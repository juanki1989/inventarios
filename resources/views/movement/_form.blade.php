
  <div class="box-body">
    <!-- INIT FORM-->
    <div class="form-group @if ($errors->has('name')) has-error @endif">
      <label>Campo</label>
      {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Ej. Hab 101 ...', 'required']) !!}
      @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
    </div>
    <div class="form-group @if ($errors->has('max_capacity')) has-error @endif">
      <label>Dueno</label>
      {!! Form::text('max_capacity', null, ['class' => 'form-control', 'placeholder' => 'Maxima Capacidad ...', 'required']) !!}
      @if ($errors->has('max_capacity')) <p class="help-block">{{ $errors->first('max_capacity') }}</p> @endif
    </div>
    <div class="form-group @if ($errors->has('description')) has-error @endif">
      <label>Comision</label>
      {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Informacion adicional ...']) !!}
      @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
    </div>
    <div class="form-group @if ($errors->has('description')) has-error @endif">
      <label>Flete</label>
      {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Informacion adicional ...']) !!}
      @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
    </div>
    <div class="form-group @if ($errors->has('description')) has-error @endif">
      <label>Importe</label>
      {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Informacion adicional ...']) !!}
      @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
    </div>
    <!-- END FORM -->
  </div>
  <!-- /.box-body -->
</div>
