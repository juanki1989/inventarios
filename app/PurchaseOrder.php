<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
  protected $table = 'purchase_orders';
  protected $fillable = ['id','price', 'flete', 'state','customer_id', 'user_id'];

  public function purchaseOrdersDetail(){
    return $this->hasMany('App\PurchaseOrderDetail','purchase_orders_id', 'id')->get();
  }
}
