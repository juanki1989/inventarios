

    <!-- INIT FORM-->
    <div class="form-group @if ($errors->has('customer_id')) has-error @endif">
      <label class='col-sm-2 control-label'>Dueño</label>
      <div class="col-sm-10">
        {!! Form::select('customer_id',$customers,
                         isset($customer) ? $purchaseOrder->customer_id : null,
                         ['data-placeholder'=>'Seleccione un estado',
                          'class'=>'form-control',
                          'required']) !!}
        @if ($errors->has('customer_id')) <p class="help-block">{{ $errors->first('customer_id') }}</p> @endif
      </div>
    </div>
    <div class="form-group @if ($errors->has('users')) has-error @endif">
      <label class='col-sm-2 control-label'>Comision</label>
      <div class="col-sm-10">
      {!! Form::select('user_id',$users,
                       isset($room) ? $purchaseOrder->user_id : null,
                       ['data-placeholder'=>'Seleccione un estado',
                        'class'=>'form-control',
                        'required']) !!}
      @if ($errors->has('users')) <p class="help-block">{{ $errors->first('users') }}</p> @endif
      </div>
    </div>
    <div class="form-group @if ($errors->has('price')) has-error @endif">
      <label class='col-sm-2 control-label'>Importe</label>
      <div class="col-sm-10">
      {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => '$ ...', 'required']) !!}
      @if ($errors->has('price')) <p class="help-block">{{ $errors->first('price') }}</p> @endif
      </div>
    </div>
    <div class="form-group @if ($errors->has('flete')) has-error @endif">
      <label class='col-sm-2 control-label'>Flete</label>
      <div class="col-sm-10">
      {!! Form::text('flete', null, ['class' => 'form-control', 'placeholder' => '...', 'required']) !!}
      @if ($errors->has('flete')) <p class="help-block">{{ $errors->first('flete') }}</p> @endif
      </div>
    </div>
    <!-- END FORM -->
