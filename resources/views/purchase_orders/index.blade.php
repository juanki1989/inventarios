@extends('admin_template')

<!-- @section('title', 'Caja Chica') -->

@section('content')
    <div class="row">
        <div class="col-md-5">
            <h3 class="modal-title">{{ $result->total() }} {{ str_plural('Cuentas', $result->count()) }} </h3>
        </div>
        <div class="col-md-7 page-action text-right">
            @can('add_users')
                <a href="{{ route('compras.create') }}" class="btn btn-primary btn-sm"> <i class="glyphicon glyphicon-plus-sign"></i> Create</a>
            @endcan
        </div>
    </div>

    <div class="result-set">
        <table class="table table-bordered table-striped table-hover" id="data-table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Precio</th>
                <th>Flete</th>
                <th class="text-center">Estado</th>
                <th>Fecha</th>

                @can('edit_purchase_orders', 'delete_purchase_orders')
                <th class="text-center">Actions</th>
                @endcan
            </tr>
            </thead>
            <tbody>
            @foreach($result as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->price }}</td>
                    <td>{{ $item->flete }}</td>
                    <td class="text-center">
                        @if($item->state == 'pendiente')
                          <span class="label label-warning">{{ $item->state }}</span>
                        @endif
                        @if($item->state == 'confirmado')
                          <span class="label label-success">{{ $item->state }}</span>
                        @endif
                    </td>
                    <td>{{ $item->created_at }}</td>
                    @can('edit_purchase_orders', 'delete_purchase_orders')
                    <td class="text-center">
                        @include('shared._actions', [
                            'entity' => 'compras',
                            'id' => $item->id
                        ])
                    </td>
                    @endcan
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="text-center">
            {{ $result->links() }}
        </div>
    </div>

@endsection
