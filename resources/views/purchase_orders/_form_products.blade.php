<div class="row">
  <div class="col-xs-12">
    <div class="table-responsive no-padding">
      <table class="table table-hover">
        <tr>
          <th>Producto</th>
          <th>Price</th>
          <th>Kg</th>
          <th>Cantidad</th>
        </tr>
        @if(isset($purchaseOrdersDetail))
          @foreach($purchaseOrdersDetail as $item)
          <tr>
            <td>{{ $item->product->name }}</td>
            <td>{{ $item->price }}</td>
            <td>{{ $item->kg }}</td>
            <td><span class="label label-success">{{ $item->cantidad }}</span></td>
          </tr>
          @endforeach
        @endif
      </table>
    </div>
  </div>
</div>
<!-- /.box-body -->
