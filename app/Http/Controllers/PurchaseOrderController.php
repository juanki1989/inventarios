<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Product;
use App\User;
use App\PurchaseOrder;
use App\PurchaseOrderDetail;

class PurchaseOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $result = PurchaseOrder::latest()->paginate();
      return view('purchase_orders.index', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::pluck('name', 'id');
        $users = User::pluck('name', 'id');
        $products = Product::pluck('name', 'id');

        $purchaseOrder = new PurchaseOrder();
        $purchaseOrder->save();
        $purchaseOrderDetail = $purchaseOrder->purchaseOrdersDetail();
        return view('purchase_orders.edit', compact('customers'))
                    ->with(compact('users'))
                    ->with(compact('products'))
                    ->with(compact('purchaseOrderDetail'))
                    ->with(compact('purchaseOrder'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $customers = Customer::pluck('name', 'id');
      $users = User::pluck('name', 'id');
      $products = Product::pluck('name', 'id');

      $purchaseOrder = PurchaseOrder::findOrFail($id);
      $purchaseOrdersDetail = $purchaseOrder->purchaseOrdersDetail();

      return view('purchase_orders.edit', compact('customers'))
                  ->with(compact('users'))
                  ->with(compact('products'))
                  ->with(compact('purchaseOrdersDetail'))
                  ->with(compact('purchaseOrder'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $purchaseOrder = PurchaseOrder::findOrFail($id);
      $purchaseOrder->update($request->all());
      $purchaseOrder->state = 'confirmado';
      $purchaseOrder->save();
      return redirect()->route('compras.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
