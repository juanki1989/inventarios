<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{ asset("/admin-lte/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>Juan Carlos</p>
        <!-- Status -->
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>

    <!-- search form (Optional) -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
      </div>
    </form>
    <!-- /.search form -->

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
      <li class="header">Menu</li>
      <!-- Optionally, you can add icons to the links -->
      <li class="{{ Request::is('home*') ? 'active' : '' }}"><a href="{{ route('home')}}"><i class="fa fa-link"></i> <span>Dashboard</span></a></li>
      @if (Auth::check())
          <li class="treeview {{ Request::is('compras*') ? 'active' : '' }}">
            <a href="#"><i class="text-info fa fa-fw fa-list"></i> <span>Compras</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{ route('compras.index')}}"><i class="text-info fa fa-fw fa-list"></i> Lista Compras</a></li>
              <li><a href="{{ route('compras.create')}}"><i class="text-info fa fa-fw fa-arrow-circle-right"></i> Crear Compra</a></li>
            </ul>
          </li>
          <li class="treeview {{ Request::is('movements*') ? 'active' : '' }}">
            <a href="#"><i class="text-info fa fa-fw fa-list"></i> <span>Movimientos</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{ route('movement.index')}}"><i class="text-info fa fa-fw fa-list"></i> Lista Movimientos</a></li>
              <li><a href="{{ route('movement.create')}}"><i class="text-info fa fa-fw fa-arrow-circle-right"></i> Crear Entrada</a></li>
              <li><a href="{{ route('movement.create')}}"><i class="text-info fa fa-fw fa-arrow-circle-left"></i> Crear Salida</a></li>
            </ul>
          </li>
          <li class="treeview {{ Request::is('movements*') ? 'active' : '' }}">
            <a href="#"><i class="text-info fa fa-fw fa-line-chart"></i> <span>Reportes</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{ route('movement.index')}}"><i class="text-info fa fa-fw fa-list"></i> Reporte de Entradas</a></li>
              <li><a href="{{ route('movement.create')}}"><i class="text-info fa fa-fw fa-arrow-circle-right"></i> Reporte de Salidas</a></li>
            </ul>
          </li>
          @can('view_users')
              <li class="treeview {{ Request::is('users*') ? 'active' : '' }}">
                <a href="#"><i class="text-info glyphicon glyphicon-user"></i> <span>Users</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="{{ route('users.index')}}">Lista</a></li>
                  <li><a href="{{ route('users.create')}}">Crear Nuevo</a></li>
                </ul>
              </li>
          @endcan

          @can('view_posts')
              <!-- <li class="{{ Request::is('posts*') ? 'active' : '' }}">
                  <a href="{{ route('posts.index') }}">
                      <i class="text-success glyphicon glyphicon-text-background"></i> <span>Posts</span>
                  </a>
              </li>-->
          @endcan
          @can('view_roles')
            <li class="{{ Request::is('roles*') ? 'active' : '' }}">
                <a href="{{ route('roles.index') }}">
                    <i class="text-danger glyphicon glyphicon-lock"></i> <span>Roles</span>
                </a>
            </li>
          @endcan

      @endif
    </ul>
    <!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>
