
<div class="row">
  <div class="col-xs-12">
    <div class="col-sm-3">
      <div class="form-group @if ($errors->has('products')) has-error @endif">
        {!! Form::select('products_id',$products,
                         isset($room) ? $purchaseOrder->products_id : null,
                         ['data-placeholder'=>'Seleccione un producto',
                          'class'=>'form-control',
                          'required']) !!}
        @if ($errors->has('products_id')) <p class="help-block">{{ $errors->first('products_id') }}</p> @endif
      </div>
    </div>

    <div class="col-sm-2">
      <div class="form-group @if ($errors->has('price')) has-error @endif">
        <div class="col-ms-3">
        {!! Form::number('price', null, ['class' => 'form-control', 'placeholder' => 'Importe', 'required']) !!}
        @if ($errors->has('price')) <p class="help-block">{{ $errors->first('price') }}</p> @endif
        </div>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group @if ($errors->has('kg')) has-error @endif">
        <div class="col-ms-3">
        {!! Form::number('kg', null, ['class' => 'form-control', 'placeholder' => 'Kilogramos', 'required']) !!}
        @if ($errors->has('kg')) <p class="help-block">{{ $errors->first('kg') }}</p> @endif
        </div>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group @if ($errors->has('cantidad')) has-error @endif">
        <div class="col-ms-3">
        {!! Form::number('cantidad', null, ['class' => 'form-control', 'placeholder' => 'Cantidad', 'required']) !!}
        @if ($errors->has('cantidad')) <p class="help-block">{{ $errors->first('cantidad') }}</p> @endif
        </div>
      </div>
    </div>
    {{ Form::hidden('purchase_orders_id', $purchaseOrder->id) }}

    <div class="col-sm-3">
      <div class="col-ms-3">
      {!! Form::submit('Agregar Producto', ['class' => 'btn btn-primary']) !!}
      </div>
    </div>

    </div>
    </div>
    <!-- END FORM -->
