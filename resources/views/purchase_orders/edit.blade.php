@extends('admin_template')

@section('content')

    <div class="row">
        <div class="col-md-5">
            <h3>Editar</h3>
        </div>
        <div class="col-md-7 page-action text-right">
            <a href="{{ route('compras.index') }}" class="btn btn-default btn-sm"> <i class="fa fa-arrow-left"></i> Back</a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Compra ID : {{ $purchaseOrder->id }}</h3>
              </div>
              <div class="box-body">
                {!! Form::model($purchaseOrder, ['method' => 'PUT',
                                                 'route' => ['compras.update',  $purchaseOrder->id ],
                                                 'class' => 'form-horizontal' ])!!}
                    @include('purchase_orders._form')
                    <div class='text-center'>
                      {!! Form::submit('Guardar cambios', ['class' => 'btn btn-primary']) !!}
                    </div>
                {!! Form::close() !!}
                <br>
                {!! Form::open(['route' => ['comprasDetalle.store'] ]) !!}
                    @include('purchase_orders._form_product')
                {!! Form::close() !!}

                @include('purchase_orders._form_products')
            </div>
          </div>
        </div>
    </div>
@endsection
