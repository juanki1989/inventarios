@extends('admin_template')

@section('content')

    <div class="row">
        <div class="col-md-5">
            <h3>Create</h3>
        </div>
        <div class="col-md-7 page-action text-right">
            <a href="{{ route('compras.index') }}" class="btn btn-default btn-sm"> <i class="fa fa-arrow-left"></i> Back</a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Nueva Categoria Habitación</h3>
            </div>
            {!! Form::open(['route' => ['compras.store'] ]) !!}
                @include('purchase_orders._form')
                <!-- Submit Form Button -->
                {!! Form::submit('Crear', ['class' => 'btn btn-primary']) !!}
            {!! Form::close() !!}
          </div>
        </div>
    </div>
@endsection
