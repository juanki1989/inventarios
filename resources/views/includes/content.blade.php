<!-- Main content -->
<section class="content">
  <div id="flash-msg">
      @include('flash::message')
  </div>
  @yield('content')
  @stack('scripts')

  <script>
      $(function () {
          // flash auto hide
          $('#flash-msg .alert').not('.alert-danger, .alert-important').delay(6000).slideUp(500);
      })
  </script>
</section>
