@extends('admin_template')

@section('content')

    <div class="row">
        <div class="col-md-5">
            <h3>Editar</h3>
        </div>
        <div class="col-md-7 page-action text-right">
            <a href="{{ route('rooms.index') }}" class="btn btn-default btn-sm"> <i class="fa fa-arrow-left"></i> Back</a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Categoria Habitación {{ $roomCategory->name }}</h3>
              </div>
            {!! Form::model($roomCategory, ['method' => 'PUT', 'route' => ['roomCategories.update',  $roomCategory->id ] ]) !!}
                @include('roomCategory._form')
                <!-- Submit Form Button -->
                {!! Form::submit('Guardar cambios', ['class' => 'btn btn-primary']) !!}
            {!! Form::close() !!}
          </div>
        </div>
    </div>
@endsection
