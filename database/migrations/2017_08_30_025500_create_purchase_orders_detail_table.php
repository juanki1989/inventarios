<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrdersDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders_detail', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('price')->nullable();
          $table->integer('kg')->nullable();
          $table->integer('cantidad')->nullable();
          $table->integer('purchase_orders_id')->nullable();
          $table->foreign('purchase_orders_id')
                ->references('id')->on('purchase_orders')->onDelete('cascade');
          $table->integer('products_id')->nullable();
          $table->foreign('products_id')
                ->references('id')->on('products')->onDelete('cascade');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders_detail');
    }
}
